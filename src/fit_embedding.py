"""
Fit set embeddings
"""
import sys
import time

import ipdb
import numpy as np
import tensorflow as tf
from numpy.random import choice
from pymanopt import Problem
from pymanopt.manifolds import Euclidean, Product
from pymanopt.solvers import ConjugateGradient
from scipy.io import mmread


class SEF():
    """
    :param V: The target matrix to estimate.
    :type V: Instance of the :class:`scipy.sparse` sparse matrices types,
       :class:`numpy.ndarray`, :class:`numpy.matrix` or tuple of instances of
       the latter classes.
    """

    def __initial_solution(self, p_c=None, p_r=None, **kwargs):
        """
        Return initialized basis and mixture matrix. Initialized matrices are
        of the same type as passed target matrix.

        :param p_c: proportion of columns of target matrix used to produce
        1 column of the initial basis matrix (default value is None, <= .9).
        :param p_r: proportion of columns of target matrix used to produce
        1 column of the initial basis matrix (default value is None, <= .9).
        """
        N, M = self.idata.shape
        p_c = int(.1 * M) if p_c is None else np.maximum(p_c, .9)
        p_r = int(.1 * M) if p_r is None else np.maximum(p_r, .9)
        cr = np.arange(N)
        rank = self.embedding_dimension
        W = np.zeros((M, rank), dtype=self.dtype)
        H = np.zeros((rank, M), dtype=self.dtype)
        for i in range(rank):
            W[:, i] = np.asarray(
                self.idata[choice(cr, p_c), :].mean(axis=0)).squeeze()
            H[i, :] = np.asarray(
                self.idata[choice(cr, p_r), :].mean(axis=0)).squeeze()
        return W, H

    def __init__(self,
                 data_filename: str,
                 embedding_dimension=100,
                 eps=None,
                 dtype="float64",
                 **kwargs):
        self.name = "SEF"
        self.dtype = dtype
        self.idata = mmread(data_filename).tocsr()
        self.idata.data = self.idata.data.astype(dtype)
        coo = self.idata.tocoo()
        self.input_data = tf.SparseTensor(
            np.mat([coo.row, coo.col]).transpose(), coo.data, coo.shape)
        self.embedding_dimension = embedding_dimension
        self.W = None
        self.H = None
        self.best_obj = None
        self.n_iter = None
        self.eps = np.finfo(self.dtype).eps if eps is None else eps

    def fit_one(self,
                verbosity=2,
                max_iter=2500,
                max_time=12 * 3600,
                mingradnorm=1e-7,
                alternative=False,
                debug=False,
                mean_objective=True,
                **kwargs):
        """run single Tensorflow optimization"""
        print("sample initial solution...")
        W0, H0 = self.__initial_solution(**kwargs)
        if debug:
            sess = tf.Session()
        norm_factor = tf.constant(W0.shape[0]**2,
                                  self.dtype) if mean_objective else 1.
        W = tf.Variable(tf.placeholder(self.dtype, W0.shape), name="W")
        H = tf.Variable(tf.placeholder(self.dtype, H0.shape), name="H")
        Ytheta = tf.matmul(tf.sparse.matmul(self.input_data, W), H)
        linear_term = tf.SparseTensor.__mul__(self.input_data, Ytheta)
        obj = (tf.log_sigmoid(Ytheta) -
               Ytheta) if alternative else -tf.log(tf.exp(Ytheta) + 1.)
        obj = -tf.reduce_sum(tf.sparse.add(linear_term, obj)) / norm_factor
        vars = [W, H]
        problem = Problem(
            manifold=Product([Euclidean(W0.shape),
                              Euclidean(H0.shape)]),
            cost=obj,
            arg=vars,
            verbosity=verbosity)
        solver = ConjugateGradient(
            maxtime=max_time,
            maxiter=max_iter,
            mingradnorm=mingradnorm,
            logverbosity=1)
        print("fitting...")
        self.solution, extra = solver.solve(problem, x=[W0, H0])
        if debug:
            feed = dict(zip(vars, self.solution))  # vars at optimal values
            print(sess.run(obj, feed))
            print(sess.run(tf.gradients(obj, vars), feed))
            A = tf.log(tf.exp(Ytheta) + 1.)
            obj1 = -tf.reduce_sum(tf.sparse.add(linear_term, -A)) / norm_factor
            print(sess.run(obj1, feed))
            print(sess.run(tf.gradients(obj1, vars), feed))
            ipdb.set_trace()
            sess.close()
        return self.solution + [
            extra['final_values']['f(x)'],
            extra['final_values']['iterations'] + 1
        ]

    def fit(self, random_seed=15621, n_run=3, verbose=2, **kwargs):
        """pick the best fit from multiple Tensorflow optimizations"""
        np.random.seed(random_seed)
        verbose = int(verbose)
        self.best_obj = sys.float_info.max
        start = time.time()
        for run in range(n_run):
            W, H, opt_obj, counter = self.fit_one(verbose, **kwargs)
            if run == 0:
                self.best_obj = opt_obj
            elif opt_obj <= self.best_obj:
                self.best_obj = opt_obj
                self.n_iter = counter
                self.W = W.copy()
                self.H = H.copy()
        print("average elapsed time: {:.2f} seconds".format(
            (time.time() - start) / n_run))
        return self

    def get_W(self):
        """Return the first layer/encoding matrix."""
        return self.W

    def get_H(self):
        """Return the second layer/decoding matrix."""
        return self.H

    def get_objective(self):
        """Return the best obective value."""
        return self.best_obj


# test
if False:
    test = SEF("data/testmat.mtx")
    res_test = test.fit(
        n_run=1, max_iter=30)  #, alternative=False, debug=True)
    res_test = test.fit(n_run=1, alternative=True, debug=True, max_iter=30)

    res = test.fit()  # final fit
